namespace Website.Helpers

open System.Web.Mvc

type DataBoundViewPage() =
    inherit ViewPage()

    override this.OnLoad e =
        base.OnLoad e
        this.DataBind()

namespace Website

// pagelets sample related types
type Item =
    {
        Id: int
        Name: string
        Description: string
    }

type AppModel =
    {
        Items: ResizeArray<Item>
    }

    static member CreateDummy() =
        let createItem i =
            {
                Id = i
                Name = sprintf "Name%d" i
                Description = sprintf "Description%d" i
            }
        let items = Seq.init 5 createItem
        {Items = ResizeArray items}

type ViewModel =
    {
        Items: Item array
    }

type AddItemResult =
    | Ok of Item
    | Error of string

// formlets sample related types
type BasicInfo =
    {
        Name: string
        Age: int
    }

type Address =
    {
        Street: string
        City: string
        Country: string
    }

type Contact =
    | PhoneContact of string
    | AddressContact of Address

namespace Website.Handlers

open IntelliFactory.WebSharper
open Website

// Rpc handler instance - target for typed JavaScript call
// Creation behavior can be overridden via SetRpcHandlerFactory
// (from IntelliFactory.WebSharper.RemotingPervasives)
type Pagelets(model : AppModel) =
    [<Rpc>]
    member this.AddItem(item : Item) =
        if model.Items |> Seq.exists (fun b -> b.Name = item.Name)
        then
            Error <| sprintf "Item with name '%s' already exists" item.Name
        else
            let newId = model.Items.Count
            let newItem =
                {
                    Id = newId
                    Name = item.Name;
                    Description = item.Description
                }
            model.Items.Add(newItem)
            Ok <| newItem

type Formlets() =
    [<Rpc>]
    member this.ProcessResults (info : BasicInfo, contact : Contact) =
        sprintf "<h1>Thank you, %s, your data was \
            successfully processed</h1>" info.Name

namespace Website

open Autofac
open System
open System.Web
open System.Web.Mvc
open System.Web.Routing
open IntelliFactory.WebSharper

type RouteInformation =
    {
        controller: string
        action: string
    }

type FavIcon =
    {
        favicon : string
    }

type Application() =
    inherit HttpApplication()

    let items = AppModel.CreateDummy()

    static member RegisterRoutes(routes : RouteCollection) =
        routes.IgnoreRoute("{resource}.axd/{*pathInfo}")
        routes.IgnoreRoute("{*favicon}", { favicon = @"(.*/)?favicon.ico(/.*)?" })

        routes.MapRoute(
            "Default", // Route name
            "{controller}/{action}", // URL with parameters
            { controller = "Home"; action = "Index"} // Parameter defaults
        )

    

    member this.Start() =
        Application.RegisterRoutes(RouteTable.Routes) |> ignore

        let container =
            let builder = new ContainerBuilder()
            builder.RegisterInstance(items) |> ignore // register model instance
            builder.RegisterType<Handlers.Pagelets>() |> ignore
            builder.RegisterType<Handlers.Formlets>() |> ignore

            // register all controllers in current assembly
            builder
                .RegisterAssemblyTypes(
                    [| System.Reflection.Assembly.GetExecutingAssembly() |])
                .AssignableTo<IController>() |> ignore
            builder.Build()

        let ctrlFactory =
            { new DefaultControllerFactory() with
                override this.GetControllerInstance(requestContext, controllerType) =
                    if controllerType = null
                    then
                        new HttpException(404,
                            sprintf "controller for path %s not found"
                                requestContext.HttpContext.Request.Path)
                        |> raise
                    else container.Resolve(controllerType : Type) :?> IController
                override this.ReleaseController(controller) = () }

        ControllerBuilder.Current.SetControllerFactory(ctrlFactory)

        let handlerFactory =
            { new IRpcHandlerFactory with
                member this.Create(handlerType) =
                    container.Resolve(handlerType) |> Some }

        SetRpcHandlerFactory(handlerFactory)
