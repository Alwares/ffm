namespace Website.Controllers

open Website
open IntelliFactory.WebSharper
open IntelliFactory.WebSharper.Html
open System.IO
open System.Text
open System.Runtime.Serialization
open FSharp.Data.Json
open System.Configuration
open Newtonsoft.Json

open System
open System.Web.Mvc
open Website
open System.Data.Entity
open System.ComponentModel.DataAnnotations
open System.Data.Linq
open Microsoft.FSharp.Data.TypeProviders
open System.Data
open System.Data.Entity
open System.Data.Linq

//open Microsoft.FSharp.Data.TypeProviders

type picCover ={
     cover_id : int
     source : string
     offset_y : int
     offset_x : int
}

type band = {
     page_id : string
     type_ :string
     global_brand_page_name : string
     pic : string
}

type root={
     data : band list
}

type music ={
    page_id : string
    description : string
    global_brand_page_name : string
    }
type video={
    title : string
    videoid : string
    ratings : float
    description  : string
}

type Band ={
    page_id : Int64
    type_ : string
    global_brand_page_name : string
    pic : string
}

type FbBands={
    data : Band seq
}

type user={
    id : Int64
    name : string
}

//    

module DbManager =
    //[<Literal>]
    //let con = ""
    //type EntityConnection = SqlDataConnection<>
    //[<NotMapped>]
    type public User()=
        let mutable m_ID : Int64 = Int64.MaxValue
        let mutable m_Name : string = ""

        [<Key>]
        member public this.ID   with    get()   = m_ID
                                and     set v = m_ID <- v

        member public this.Name with    get()   = m_Name
                                and     set v = m_Name <- v
    type public UserDb()=
        inherit DbContext()
    
        [<DefaultValue>]
        val mutable m_user : DbSet<User>
        member public this.Users    with    get()   = this.m_user
                                    and     set v   = this.m_user <- v

    let SaveUser ( id : string, name : string ) =
        let db = new UserDb()
        let user = new User(ID = Int64.Parse(id), Name = name )
        db.Users.Add(user) |> ignore
        db.SaveChanges() |> ignore
        true

    let LoginUser( fbuser : user) =

        true

module YouTube =     
    module Handler =
        let Request = 
            let AccesToken = "AI39si5K2LzTsjPWfofg3Ic8MSTKMhw9j2NaafLyJXpZJFx4ZVprehElTC2WtLvpTQ37E62IkzsCEgEHOKPWtCYX9GX91dJ9ZA"
            
            let YtSettings = new Google.YouTube.YouTubeRequestSettings("my.fm", AccesToken)
            let rq = new Google.YouTube.YouTubeRequest(YtSettings)
            rq
        let GetVideo (query : string) =
            let feed ="https://gdata.youtube.com/feeds/api/videos?q=" + query + "+-studio&orderby=relevance&max-results=1&category=Music&duration=short"
            let uri = new System.Uri(feed)
            let videoFeed = Request.Get<Google.YouTube.Video>(uri)
            let next = List.ofSeq : seq<'T> -> 'T list
            let lists = next videoFeed.Entries
            "http://www.youtube.com/embed/" +  lists.Head.Media.VideoId.Value + "?enablejsapi=1&html5=1&autoplay=1"  
    let GetVideoForBand ( bands : Band seq, index : int) =
        let hasnext = bands.GetEnumerator().MoveNext()
        //let current = List.ofSeq bands
        let lista = List.ofSeq bands
        let current = lista.Item index
        Handler.GetVideo current.global_brand_page_name
    

//A Facebook Api lek�rdez�sekhez hozz� kell igaz�tani az App-ban az alakalmaz�s fut�si k�rnyezet�t a Facebook Developers fel�leten (pl fut�siurl.com)
module Facebook =
    let GetBands (token : string) =
        let select = "https://graph.facebook.com/fql?q=SELECT page_id,type, global_brand_page_name, pic FROM page WHERE page_id IN (SELECT uid, page_id, type FROM page_fan WHERE uid=me()) AND type='musician/band'&access_token=" + token
        let webclient = new System.Net.WebClient()
        let res = webclient.DownloadString(select).Replace("type","type_")

        let out = JsonConvert.DeserializeObject<FbBands>(res)

        out.data
    let LoginUser( id : string, accessToken : string)=
        let apiKey = "440170366070316"
        let appSecret = "7b4f3b60a3360214e1350c379cba4a92"
        let select = "https://graph.facebook.com/"+id+"?&access_token=" + accessToken
        let webclient = new System.Net.WebClient()
        let res = webclient.DownloadString(select) 
        let out = JsonConvert.DeserializeObject<user>(res)
        true
    module Access = 
        let GetAccesToken ( clientid : string ) =
            let apiKey = "440170366070316"
            let appSecret = "7b4f3b60a3360214e1350c379cba4a92"
            let select = "https://graph.facebook.com/oauth/access_token?client_id="+apiKey+"&client_secret="+appSecret+"&grant_type=client_credentials"
            let webclient = new System.Net.WebClient()
            let res = webclient.DownloadString(select).Replace("type","type_")
            res

[<HandleError>]
type HomeController() =
    inherit Controller()

    member this.Index() =
        match this.Session.["accessToken"] with 
        | null -> ()
        | _ -> 
            match this.Session.["bands"] with
            | null ->
                let bands = Facebook.GetBands
                this.Session.["bands"] <- bands
                let token = (string)this.Session.["accessToken"]
                this.ViewData.["YoutubeLink"] <- YouTube.GetVideoForBand <| (bands token, 0)
            | _ ->
                let token = string this.Session.["accessToken"]
                let bandsobj = this.Session.["bands"]
                //let bands : seq<Band> = bandsobj as seq<Band> :>
                let bands = Facebook.GetBands
                this.ViewData.["YoutubeLink"] <- YouTube.GetVideoForBand <| (bands token, 0)

        this.View()

    member this.My(token : string) =
        this.ViewData.["FbData"] <- YouTube.GetVideoForBand <| (Facebook.GetBands token,0)
        this.View("Index")
    member this.Next(index : int)=
        let token = (string)this.Session.["accessToken"]
        YouTube.GetVideoForBand <| (Facebook.GetBands token,index)

    member this.GetVideo(index : int)=
        let token = (string)this.Session.["accessToken"]
        YouTube.GetVideoForBand <| (Facebook.GetBands token,index)

[<HandleError>]
type FormletsController() =
    inherit Controller()

    member this.Index() =
        this.View()

[<HandleError>]
type PageletsController(model : AppModel) =
    inherit Controller()

    member this.Items() =
        let viewModel : ViewModel = {Items = model.Items |> Array.ofSeq}
        this.View(viewModel)

type FbloginModel={
    uid : string
    accessToken : string
}

[<HandleError>]
type ProfileController()=
    inherit Controller()

    member this.Index() =
        this.View()

    //[<HttpPost>]
//    member this.FacebookLogin( model: FbloginModel )=
//        this.Session.["accessToken"] <- model.accessToken
//        this.Session.["uid"] <- model.uid
//        true
    member this.FacebookLogin(uid: string, accessToken : string)=
        this.Session.["accessToken"] <- accessToken
        this.Session.["uid"] <- uid
        //let saved = Facebook.LoginUser <| (uid,accessToken)
        true


