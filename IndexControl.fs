namespace Website

open IntelliFactory.WebSharper
open IntelliFactory.WebSharper.Html

open Website

type IndexControl() =
    inherit Web.Control()

    [<DefaultValue>]
    val mutable Message : string

    [<JavaScript>]
    override this.Body =
        let wrap href = Div [ Text "[ "; href; Text " ]" ]
//        let pageletsLink =
//            wrap <| A [ HRef "/Pagelets/Items" ] -< [Text "Pagelets example"]
//        let formletsLink =
//            wrap <| A [ HRef "/Formlets" ] -< [Text "Formlets example"]
        upcast Div [
            H2 [Text this.Message ]
//            P [ pageletsLink ]
//            P [ formletsLink ]
        ]
