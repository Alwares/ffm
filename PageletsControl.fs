namespace Website

#nowarn "40"

open IntelliFactory.WebSharper
open IntelliFactory.WebSharper.Html
open IntelliFactory.WebSharper.Formlet
open IntelliFactory.WebSharper.Remoting
open Website
open System.Collections.Generic

module Pagelets =

    [<JavaScript>]
    let main (model : ViewModel) =
        let itemToAmount = new Dictionary<int, int>()
        let createItemRow (item : Item) =

            let amount = Div [Text ""]

            let rec plus =
                Button [Text "+"]
                |>! OnClick (fun _ _ ->
                    let oldValue = itemToAmount.[item.Id]
                    setAmount (oldValue + 1))
            and minus =
                Button [Text "-"]
                |>! OnClick (fun _ _ ->
                    let oldValue = itemToAmount.[item.Id]
                    setAmount (oldValue - 1)
                    )
            and setAmount v =
                if v = 0
                then
                    minus.SetAttribute("disabled", "true")
                else
                    minus.RemoveAttribute("disabled")
                itemToAmount.[item.Id] <- v
                amount.Text <- v.ToString()

            setAmount 0

            TR [
                TD [Text item.Name]
                TD [Text item.Description]
                TD [plus]
                TD [minus]
                TD [amount]
            ]


        let items =
            Table [
                yield TR [
                    TD [Attr.Class "tableCaption"] -< [Text "Name"]
                    TD [Attr.Class "tableCaption"] -< [Text "Description"];
                    TD [];
                    TD [];
                    TD [Attr.Class "tableCaption"] -< [ Text "Amount" ]
                    ]
                for item in model.Items do
                    yield createItemRow item
            ]

        let statusBar = Div []
        let setStatus text success =
            statusBar.Text <- text
            let statusClass = if success then "okStatus" else "errorStatus"
            statusBar.AddClass(statusClass)
            let jq = JQuery.JQuery.Of(statusBar.Dom)
            jq.FadeIn(800.0, fun () ->
                jq.FadeOut(800.0, fun () ->
                    statusBar.RemoveClass(statusClass)
                    ) |> ignore
                ) |> ignore

        let addNewItem =
            let make caption =
                Controls.Input ""
                |> Validator.IsNotEmpty (caption + " should be set")
                |> Enhance.WithValidationIcon
                |> Enhance.WithTextLabel caption

            Formlet.Yield (fun name description -> name, description)
                <*> (make "Name")
                <*> (make "Description")
                |> Enhance.WithSubmitButton
                |> Enhance.WithLegend "Add new item"
                |> Formlet.Run (fun (name, description) ->
                        let item = { Id = 0; Name = name; Description = description}
                        let result = Remote<Handlers.Pagelets>.AddItem(item)
                        match result with
                        | Ok item ->
                            items.Append(createItemRow item)
                            setStatus "Item was added successfully"  true
                        | Error message ->
                            setStatus message false
                    )
        Table [
            TR [
                TD [ FieldSet [ Legend [ Text "Select items" ]; items] ]
                TD [ VAlign "top"] -< [ addNewItem; upcast statusBar ]
            ]
        ]

type PageletsControl() =
    inherit Web.Control()

    [<DefaultValue>]
    val mutable Items :  ViewModel

    [<JavaScript>]
    override this.Body = upcast (Pagelets.main this.Items)
