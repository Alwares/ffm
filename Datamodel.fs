﻿namespace DataModel

    open Website
    open System
    open System.Data.Entity
    open System.ComponentModel.DataAnnotations

    type public User()=
        let mutable m_ID : Int64 = Int64.MaxValue
        let mutable m_Name : string = ""

        [<Key>]
        member public this.ID   with    get()   = m_ID
                                and     set v = m_ID <- v
        member public this.Name with    get()   = m_Name
                                and     set v = m_Name <- v

    type public UserDb()=
        inherit DbContext()
    
        [<DefaultValue>]
        val mutable m_user : DbSet<User>
        member public this.Users    with    get()   = this.m_user
                                    and     set v   = this.m_user <- v